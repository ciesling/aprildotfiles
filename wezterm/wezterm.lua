local wezterm = require 'wezterm'

local launch_menu = {}
local default_prog = {}
local set_environment_variables = {}
if wezterm.target_triple == 'x86_64-unknown-linux-gnu' then
    table.insert( launch_menu, {
        label = 'Zsh',
        args = { 'zsh', '-l' }
     } )
    default_prog = { 'zsh', '-l' }
  end
-- Title
function basename( s )
    return string.gsub( s, '(.*[/\\])(.*)', '%2' )
end

wezterm.on( 'format-tab-title', function( tab, tabs, panes, config, hover, max_width )
    local pane = tab.active_pane

    local index = ""
    if #tabs > 1 then
        index = string.format( "%d: ", tab.tab_index + 1 )
    end

    local process = basename( pane.foreground_process_name )

    return { {
        Text = ' ' .. index .. process .. ' '
     } }
end )

-- Startup
wezterm.on( 'gui-startup', function( cmd )
    local tab, pane, window = wezterm.mux.spawn_window( cmd or {} )
    window:gui_window():maximize()
end )

local config = {
    -- Basic
    check_for_updates = false,
    switch_to_last_active_tab_when_closing_tab = false,
    enable_scroll_bar = true,
    window_close_confirmation = "NeverPrompt",
    -- Window
    native_macos_fullscreen_mode = false,
    adjust_window_size_when_changing_font_size = true,
    window_background_opacity = 0.96,
    window_padding = {
        left = 5.0,
        right = 5.0,
        top = 5.0,
        bottom = 5.0
     },
    window_background_image_hsb = {
        brightness = 0.8,
        hue = 1.0,
        saturation = 1.0
     },

    -- Font
    font = wezterm.font_with_fallback { 'Operator Mono Lig' },
    font_size = 16,

    -- Tab bar
    enable_tab_bar = false,
    hide_tab_bar_if_only_one_tab = true,
    show_tab_index_in_tab_bar = false,
    tab_bar_at_bottom = true,
    tab_max_width = 25,

    -- Keys
    disable_default_key_bindings = false,
    leader = {
        key = 'w',
        mods = 'CTRL'
     },

    -- Allow using ^ with single key press.
    use_dead_keys = false,

    color_scheme = 'Monokai (base16)',

    inactive_pane_hsb = {
        hue = 1.0,
        saturation = 1.0,
        brightness = 1.0
     },

    default_prog = default_prog,
    set_environment_variables = set_environment_variables,
    launch_menu = launch_menu
 }

return config