return {
  'vimwiki/vimwiki',
  'connorholyday/vim-snazzy',
  {'akinsho/bufferline.nvim', version = "*", dependencies = 'nvim-tree/nvim-web-devicons'},
}
